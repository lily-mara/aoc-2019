use std::convert::TryFrom;
use std::fmt::Debug;
use std::ops::{DivAssign, Neg, Rem};
use std::str::FromStr;

pub fn parse_lines<T>(input: &str) -> Vec<T>
where
    T: FromStr,
    <T as FromStr>::Err: Debug,
{
    input.lines().map(|l| l.parse().unwrap()).collect()
}

pub fn parse_commas<T>(input: &str) -> Vec<T>
where
    T: FromStr,
    <T as FromStr>::Err: Debug,
{
    input.split(',').map(|l| l.parse().unwrap()).collect()
}

pub fn all_digits<T>(input: &str) -> Vec<T>
where
    T: FromStr,
    <T as FromStr>::Err: Debug,
{
    let re = regex::Regex::new("-?\\d+").unwrap();

    re.find_iter(input)
        .map(|x| x.as_str().parse().unwrap())
        .collect()
}

pub fn parse_lines_and_commas<T>(input: &str) -> Vec<Vec<T>>
where
    T: FromStr,
    <T as FromStr>::Err: Debug,
{
    input
        .lines()
        .map(|line| line.split(',').map(|l| l.parse().unwrap()).collect())
        .collect()
}

pub fn parse_lines_and_split<T>(input: &str, split: char) -> Vec<Vec<T>>
where
    T: FromStr,
    <T as FromStr>::Err: Debug,
{
    input
        .lines()
        .map(|line| line.split(split).map(|l| l.parse().unwrap()).collect())
        .collect()
}

/// Iterate over the digits of a number from least significant to most significant.
///
/// ```ignore
/// let mut iter = DigitIter(4321);
///
/// assert_eq!(iter.next().unwrap(), 1);
/// assert_eq!(iter.next().unwrap(), 2);
/// assert_eq!(iter.next().unwrap(), 3);
/// assert_eq!(iter.next().unwrap(), 4);
/// ```
pub struct DigitIter<T>(pub T);

impl<T> Iterator for DigitIter<T>
where
    <T as TryFrom<u8>>::Error: Debug,
    T: Rem<T, Output = T>
        + Copy
        + DivAssign<T>
        + TryFrom<u8>
        + PartialEq<T>
        + PartialOrd
        + Neg<Output = T>,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.0 < T::try_from(0).unwrap() {
            self.0 = -self.0;
        }

        if self.0 == T::try_from(0).unwrap() {
            return None;
        }

        let digit = self.0 % T::try_from(10).unwrap();
        self.0 /= T::try_from(10).unwrap();

        Some(digit)
    }
}
