use aoc_runner_derive::{aoc, aoc_generator};

type Value = Vec<i32>;

const WIDTH: usize = 25;
const HEIGHT: usize = 6;

#[aoc_generator(day8)]
pub fn gather_input(input: &str) -> Value {
    input
        .chars()
        .map(|z| z.to_digit(10).unwrap() as i32)
        // .chunks(WIDTH * HEIGHT)
        // .into_iter()
        // .map(|x| {
        //     x.chunks(HEIGHT)
        //         .into_iter()
        //         // .map(|y: ()| vec![]/*y.chunks(WIDTH).into_iter().collect::<Vec<i32>>()*/)
        //         .collect::<Vec<Vec<i32>>>()
        // })
        .collect()
}

#[aoc(day8, part1)]
pub fn part1(input: &Value) -> usize {
    let mut layer_id = 0;
    let mut min_2_count = usize::max_value();

    let layers = input
        .chunks(WIDTH * HEIGHT)
        .into_iter()
        .collect::<Vec<&[i32]>>();

    for (idx, layer) in layers.iter().enumerate() {
        let count = layer.iter().filter(|x| **x == 0).count();

        if count < min_2_count {
            min_2_count = count;
            layer_id = idx;
        }
    }

    let ones = layers[layer_id].iter().filter(|x| **x == 1).count();
    let twos = layers[layer_id].iter().filter(|x| **x == 2).count();

    ones * twos
}

#[aoc(day8, part2)]
pub fn part2(input: &Value) -> String {
    let mut image = vec![0; WIDTH * HEIGHT];

    let mut layers = input.chunks(WIDTH * HEIGHT).collect::<Vec<&[i32]>>();
    layers.reverse();

    for layer in layers {
        for (idx, pixel) in layer.iter().enumerate() {
            let pixel = *pixel;
            if pixel == 0 || pixel == 1 {
                image[idx] = pixel;
            }
        }
    }

    let mut image_str = format!("\n");

    for row in image.chunks(WIDTH) {
        for pix in row {
            if *pix == 0 {
                image_str.push(' ');
            }
            if *pix == 1 {
                image_str.push('■');
            }
        }
        image_str.push('\n');
    }

    image_str
}

#[cfg(test)]
mod tests {
    const PART2_ANSWER: &str = concat!(
        "\n",
        "■■■■ ■   ■■■■  ■    ■  ■ \n",
        "   ■ ■   ■■  ■ ■    ■  ■ \n",
        "  ■   ■ ■ ■■■  ■    ■■■■ \n",
        " ■     ■  ■  ■ ■    ■  ■ \n",
        "■      ■  ■  ■ ■    ■  ■ \n",
        "■■■■   ■  ■■■  ■■■■ ■  ■ \n"
    );

    tests!(8, 2480, PART2_ANSWER);

    test_examples!(
        (
            // "" => 0,
            // "" => 0,
        ),
        (
            // "" => 0,
            // "" => 0,
        )
    );
}
