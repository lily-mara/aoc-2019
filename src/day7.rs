use crate::intcode;
use crate::util;
use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;

type Value = Vec<intcode::Value>;

#[aoc_generator(day7)]
pub fn gather_input(input: &str) -> Value {
    util::parse_commas(input)
}

#[aoc(day7, part1)]
pub fn part1(input: &Value) -> intcode::Value {
    (0..=4)
        .permutations(5)
        .map(|combo| {
            let mut last = 0;

            for i in combo {
                let mut program = input.clone();

                let mut state = intcode::State::new(&mut program);
                state.run(vec![i, last]).unwrap();

                last = state.outputs[0];
            }

            last
        })
        .max()
        .unwrap()
}

#[aoc(day7, part2)]
pub fn part2(input: &Value) -> intcode::Value {
    (5..=9)
        .permutations(5)
        .map(|combo| {
            let mut inputs: Vec<Vec<intcode::Value>> = Vec::new();
            let mut outputs: Vec<Vec<intcode::Value>> = Vec::new();
            let mut states = Vec::new();
            let mut memories = Vec::new();

            for i in combo {
                inputs.push(vec![i]);
                memories.push(input.clone());
                outputs.push(Vec::new());
            }

            for m in &mut memories {
                states.push(intcode::State::new(m));
            }

            inputs[0].push(0);

            let mut needs_continue = true;
            while needs_continue {
                if inputs.iter().all(|x| x.is_empty()) {
                    panic!("All input vecs are empty");
                }

                needs_continue = false;
                for (idx, state) in states.iter_mut().enumerate() {
                    if state.run(inputs[idx].clone()).is_err() {
                        needs_continue = true;
                    }
                    inputs[idx].clear();
                    for value in state.outputs.drain(..) {
                        inputs[(idx + 1) % 5].push(value);
                        outputs[idx].push(value);
                    }
                }
            }

            *outputs[4].last().unwrap()
        })
        .max()
        .unwrap()
}

#[cfg(test)]
mod tests {
    tests!(7, 22012, 4039164);

    test_examples!(
        (
            "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0" => 43210,
            "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0" => 54321,
        ),
        (
            "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5" => 139629729,
            "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10" => 18216,
        )
    );
}
