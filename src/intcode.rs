use crate::iter::RepeatIterExt;
use crate::util::DigitIter;

pub type Value = i64;

#[derive(Debug)]
pub struct NeedsInput;

const TRUE: Value = 1;
const FALSE: Value = 0;

#[derive(PartialEq, Debug)]
pub enum Instruction {
    Add { x: Op, y: Op, out: Op },
    Mul { x: Op, y: Op, out: Op },
    Output { op: Op },
    Input { op: Op },
    JumpIfTrue { op: Op, jmp: Op },
    JumpIfFalse { op: Op, jmp: Op },
    LessThan { x: Op, y: Op, stor: Op },
    Equals { x: Op, y: Op, stor: Op },
    RelBaseAdjust { x: Op },
    Exit,
}

#[derive(PartialEq, Debug)]
pub struct Op {
    kind: OpKind,
    index: usize,
}

#[derive(PartialEq, Debug, Clone)]
pub enum OpKind {
    Immediate,
    Positional,
    Relative,
}

pub struct State<'a> {
    memory: &'a mut Vec<Value>,
    pub outputs: Vec<Value>,
    instruction_pointer: usize,
    just_jumped: bool,
    relative_base: Value,
}

impl OpKind {
    fn value(self, index: usize) -> Op {
        Op { kind: self, index }
    }
}

impl<'a> State<'a> {
    pub fn new(memory: &'a mut Vec<Value>) -> Self {
        Self {
            instruction_pointer: 0,
            memory,
            relative_base: 0,
            outputs: Vec::new(),
            just_jumped: false,
        }
    }
}

impl<'a> State<'a> {
    pub fn run<I>(&mut self, inputs: I) -> Result<(), NeedsInput>
    where
        I: IntoIterator<Item = Value>,
    {
        let mut inputs = inputs.into_iter();
        loop {
            let instruction = self.next_instruction();

            if instruction == Instruction::Exit {
                break;
            }

            let size = instruction.size();

            self.execute(instruction, &mut inputs)?;

            self.incr_ip(size);
        }

        Ok(())
    }

    fn incr_ip(&mut self, size: usize) {
        if self.just_jumped {
            self.just_jumped = false;
        } else {
            self.instruction_pointer += size;
        }
    }

    fn next_instruction(&self) -> Instruction {
        Instruction::from_memory(&self.memory, self.instruction_pointer)
    }

    fn get<'b>(&'b mut self, op: Op) -> &'b mut Value {
        match &op.kind {
            OpKind::Immediate => self.index(op.index),
            OpKind::Positional => {
                let idx = *self.index(op.index);

                self.index(idx as usize)
            }
            OpKind::Relative => {
                let base = self.relative_base as usize;
                let idx = *self.index(op.index);

                self.index((idx + base as Value) as usize)
            }
        }
    }

    fn index<'b>(&'b mut self, index: usize) -> &'b mut Value {
        if index > self.memory.len() {
            self.memory.resize(index * 2, 0);
        }

        &mut self.memory[index]
    }

    fn execute<I>(&mut self, instruction: Instruction, inputs: &mut I) -> Result<(), NeedsInput>
    where
        I: Iterator<Item = Value>,
    {
        match instruction {
            Instruction::Add { x, y, out } => {
                let x = *self.get(x);
                let y = *self.get(y);

                *self.get(out) = x + y;
            }
            Instruction::Mul { x, y, out } => {
                let x = *self.get(x);
                let y = *self.get(y);

                *self.get(out) = x * y;
            }
            Instruction::Input { op } => {
                *self.get(op) = inputs.next().ok_or(NeedsInput)?;
            }
            Instruction::Output { op } => {
                let value = *self.get(op);
                self.outputs.push(value);
            }
            Instruction::JumpIfTrue { op, jmp } => {
                if *self.get(op) != FALSE {
                    self.jump(jmp);
                }
            }
            Instruction::JumpIfFalse { op, jmp } => {
                if *self.get(op) == FALSE {
                    self.jump(jmp);
                }
            }
            Instruction::LessThan { x, y, stor } => {
                let value = if *self.get(x) < *self.get(y) {
                    TRUE
                } else {
                    FALSE
                };

                *self.get(stor) = value;
            }
            Instruction::Equals { x, y, stor } => {
                let value = if *self.get(x) == *self.get(y) {
                    TRUE
                } else {
                    FALSE
                };

                *self.get(stor) = value;
            }
            Instruction::RelBaseAdjust { x } => {
                let value = *self.get(x);

                self.relative_base += value;
            }
            Instruction::Exit => panic!("Exit instruction executed"),
        }

        Ok(())
    }

    fn jump(&mut self, jmp: Op) {
        let location = *self.get(jmp);
        self.instruction_pointer = location as usize;
        self.just_jumped = true;
    }
}

impl Instruction {
    fn from_memory(memory: &[Value], index: usize) -> Instruction {
        let value = memory[index];
        let mut kinds = DigitIter(value)
            .skip(2)
            .map(|x| match x {
                0 => OpKind::Positional,
                1 => OpKind::Immediate,
                2 => OpKind::Relative,
                x => panic!("Invalid param kind {}", x),
            })
            .then_repeat(OpKind::Positional);

        match value % 100 {
            99 => Instruction::Exit,
            1 => Instruction::Add {
                x: kinds.next_static().value(index + 1),
                y: kinds.next_static().value(index + 2),
                out: kinds.next_static().value(index + 3),
            },
            2 => Instruction::Mul {
                x: kinds.next_static().value(index + 1),
                y: kinds.next_static().value(index + 2),
                out: kinds.next_static().value(index + 3),
            },
            3 => Instruction::Input {
                op: kinds.next_static().value(index + 1),
            },
            4 => Instruction::Output {
                op: kinds.next_static().value(index + 1),
            },
            5 => Instruction::JumpIfTrue {
                op: kinds.next_static().value(index + 1),
                jmp: kinds.next_static().value(index + 2),
            },
            6 => Instruction::JumpIfFalse {
                op: kinds.next_static().value(index + 1),
                jmp: kinds.next_static().value(index + 2),
            },
            7 => Instruction::LessThan {
                x: kinds.next_static().value(index + 1),
                y: kinds.next_static().value(index + 2),
                stor: kinds.next_static().value(index + 3),
            },
            8 => Instruction::Equals {
                x: kinds.next_static().value(index + 1),
                y: kinds.next_static().value(index + 2),
                stor: kinds.next_static().value(index + 3),
            },
            9 => Instruction::RelBaseAdjust {
                x: kinds.next_static().value(index + 1),
            },
            x => panic!("Invalid instruction: {}", x),
        }
    }

    fn size(&self) -> usize {
        match self {
            Self::Add { .. } | Self::Mul { .. } | Self::LessThan { .. } | Self::Equals { .. } => 4,
            Self::Output { .. } | Self::Input { .. } => 2,
            Self::JumpIfTrue { .. } | Self::JumpIfFalse { .. } => 3,
            Self::RelBaseAdjust { .. } => 2,
            Self::Exit => 1,
        }
    }
}
