use crate::intcode;
use crate::util::parse_commas;
use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day5)]
pub fn gather_input(input: &str) -> Vec<intcode::Value> {
    parse_commas(input)
}

#[aoc(day5, part1)]
pub fn part1(program: &Vec<intcode::Value>) -> intcode::Value {
    let mut program = program.clone();

    let mut state = intcode::State::new(&mut program);
    state.run(vec![1]).unwrap();

    state
        .outputs
        .into_iter()
        .filter(|x| *x != 0)
        .nth(0)
        .unwrap()
}

#[aoc(day5, part2)]
pub fn part2(program: &Vec<intcode::Value>) -> intcode::Value {
    let mut program = program.clone();

    let mut state = intcode::State::new(&mut program);
    state.run(vec![5]).unwrap();

    state
        .outputs
        .into_iter()
        .filter(|x| *x != 0)
        .nth(0)
        .unwrap()
}

#[cfg(test)]
mod tests {
    tests!(5, 12234644, 3508186);
}
