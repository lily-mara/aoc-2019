use crate::util;
use aoc_runner_derive::{aoc, aoc_generator};
use approx::AbsDiffEq;

type Value = f64;
type Input = Vec<(Value, Value)>;

#[aoc_generator(day10)]
pub fn gather_input(input: &str) -> Input {
    let mut coords = Vec::new();

    for (y, line) in util::parse_lines::<String>(input).into_iter().enumerate() {
        for (x, c) in line.chars().enumerate() {
            if c == '#' {
                coords.push((x as Value, y as Value));
            }
        }
    }

    coords
}

#[aoc(day10, part1)]
pub fn part1(input: &Input) -> i64 {
    input
        .iter()
        .map(|point| count_viewable(point, input))
        .max()
        .unwrap()
}

fn count_viewable((x1, y1): &(Value, Value), input: &Input) -> i64 {
    let mut angles = Vec::new();
    let mut count = 0;

    'outer: for (x2, y2) in input {
        if x1 == x2 && y1 == y2 {
            continue 'outer;
        }

        let x_diff = x2 - x1;
        let y_diff = y2 - y1;

        let angle = x_diff.atan2(y_diff);

        for existing_angle in &angles {
            if Value::abs_diff_eq(&angle, &existing_angle, Value::default_epsilon()) {
                continue 'outer;
            }
        }

        angles.push(angle);
        count += 1;
    }

    count
}

#[derive(Debug)]
struct PointInfo {
    x: Value,
    y: Value,
    angle: Value,
}

#[aoc(day10, part2)]
pub fn part2(input: &Input) -> Value {
    let mut max_point = (0.0, 0.0);
    let mut max_viewable = 0;

    for point in input {
        let viewable = count_viewable(point, input);
        if viewable > max_viewable {
            max_viewable = viewable;
            max_point = point.clone();
        }
    }

    let mut infos = Vec::new();
    let (x1, y1) = max_point;

    for (x2, y2) in input {
        if x1 == *x2 && y1 == *y2 {
            continue;
        }

        infos.push(PointInfo {
            x: *x2,
            y: *y2,
            angle: (y1 - y2).atan2(x2 - x1),
        });
    }
    infos.sort_by(|a, b| a.angle.partial_cmp(&b.angle).unwrap());
    infos.reverse();

    let mut idx: usize = 0;
    for (i, info) in infos.iter().enumerate() {
        if info.angle <= std::f64::consts::PI / 2.0 && info.angle > 0.0 {
            idx = i;
            break;
        }
    }

    let first_deleted = infos.remove(idx);
    let mut last_angle = first_deleted.angle;
    let mut deleted = 1;

    while deleted < 199 {
        let angle = infos[idx].angle;

        if Value::abs_diff_eq(&angle, &last_angle, Value::default_epsilon()) {
            idx = (idx + 1) % infos.len();
        } else {
            deleted += 1;
            infos.remove(idx);
            idx %= infos.len();
        }

        last_angle = angle;
    }

    let deleted = &infos[idx];

    deleted.x * 100.0 + deleted.y
}

#[cfg(test)]
mod tests {
    // tests!(10, 0, 0);
    const EXAMPLE_INPUT: &str = ".#..#
.....
#####
....#
...##
";

    const EXAMPLE_INPUT2: &str = ".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##";

    test_examples!(
        (
            EXAMPLE_INPUT => 8,
        ),
        (
            EXAMPLE_INPUT2 => 802.0,
            // "" => 0,
        )
    );
}
