use std::str::FromStr;

#[derive(Debug, Clone, PartialEq)]
pub struct DirectionAndMagnitude {
    pub direction: Direction,
    pub magnitude: i32,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

pub fn flatten_directions(
    directions: Vec<DirectionAndMagnitude>,
) -> impl Iterator<Item = Direction> {
    directions
        .into_iter()
        .flat_map(|dir_mag| std::iter::repeat(dir_mag.direction).take(dir_mag.magnitude as usize))
}

impl FromStr for DirectionAndMagnitude {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, ()> {
        let direction = match &s[0..1] {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "L" => Direction::Left,
            "R" => Direction::Right,
            x => panic!("Invalid direction: {}", x),
        };

        Ok(Self {
            direction,
            magnitude: s[1..].parse().unwrap(),
        })
    }
}

impl Direction {
    pub fn ccw(&mut self) {
        let new = match self {
            Direction::Down => Direction::Right,
            Direction::Left => Direction::Down,
            Direction::Up => Direction::Left,
            Direction::Right => Direction::Up,
        };

        *self = new;
    }

    pub fn cw(&mut self) {
        let new = match self {
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
            Direction::Up => Direction::Right,
            Direction::Right => Direction::Down,
        };

        *self = new;
    }
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]
pub struct Coordinate {
    pub x: i32,
    pub y: i32,
}

impl Coordinate {
    pub fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    pub fn origin() -> Self {
        Self::new(0, 0)
    }

    pub fn manhattan_distance(&self, other: &Self) -> i32 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }

    pub fn translate(&mut self, direction: Direction) {
        match direction {
            Direction::Left => self.x -= 1,
            Direction::Right => self.x += 1,
            Direction::Up => self.y -= 1,
            Direction::Down => self.y += 1,
        }
    }
}
