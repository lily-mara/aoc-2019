pub struct RepeatIter<I, T> {
    iter: I,
    repeat: T,
}

pub trait RepeatIterExt<I, T> {
    /// Return an iterator that composes this iterator, and returns the same
    /// value when this iterator runs out of values
    fn then_repeat(self, repeat: T) -> RepeatIter<I, T>;
}

impl<I, T> RepeatIterExt<I, T> for I
where
    I: Iterator<Item = T>,
    T: Clone,
{
    fn then_repeat(self, repeat: T) -> RepeatIter<I, T> {
        RepeatIter { iter: self, repeat }
    }
}

impl<I, T> Iterator for RepeatIter<I, T>
where
    I: Iterator<Item = T>,
    T: Clone,
{
    type Item = T;

    fn next(&mut self) -> Option<T> {
        Some(self.next_static())
    }
}

impl<I, T> RepeatIter<I, T>
where
    I: Iterator<Item = T>,
    T: Clone,
{
    pub fn next_static(&mut self) -> T {
        self.iter.next().unwrap_or_else(|| self.repeat.clone())
    }
}
