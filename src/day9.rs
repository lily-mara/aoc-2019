use crate::intcode;
use crate::util;
use aoc_runner_derive::{aoc, aoc_generator};

type Value = Vec<intcode::Value>;

#[aoc_generator(day9)]
pub fn gather_input(input: &str) -> Value {
    util::parse_commas(input)
}

#[aoc(day9, part1)]
pub fn part1(input: &Value) -> intcode::Value {
    let mut program = input.clone();

    let mut state = intcode::State::new(&mut program);
    state.run(vec![1]).unwrap();

    state.outputs[0]
}

#[aoc(day9, part2)]
pub fn part2(input: &Value) -> intcode::Value {
    let mut program = input.clone();

    let mut state = intcode::State::new(&mut program);
    state.run(vec![2]).unwrap();

    state.outputs[0]
}

#[cfg(test)]
mod tests {
    tests!(9, 3598076521, 90722);

    #[test]
    fn test_quine() {
        use super::*;

        let input = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";

        let mut program = gather_input(input);
        let p = program.clone();

        let mut state = intcode::State::new(&mut program);
        state.run(vec![]).unwrap();

        assert_eq!(state.outputs, p);
    }

    test_examples!(
        (
            "104,1125899906842624,99" => 1125899906842624,
            "1102,34915192,34915192,7,4,7,99,0" => 1219070632396864,
        ),
        (
            // "" => 0,
            // "" => 0,
        )
    );
}
