use crate::intcode;
use crate::util::parse_commas;
use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day2)]
pub fn gather_input(input: &str) -> Vec<intcode::Value> {
    parse_commas(input)
}

#[aoc(day2, part1)]
pub fn part1(input: &Vec<intcode::Value>) -> intcode::Value {
    let mut input = input.clone();

    intcode_process(&mut input, 12, 2)
}

fn intcode_process(
    input: &mut Vec<intcode::Value>,
    noun: intcode::Value,
    verb: intcode::Value,
) -> intcode::Value {
    input[1] = noun;
    input[2] = verb;

    let mut state = intcode::State::new(input);
    state.run(vec![]).unwrap();

    input[0]
}

#[aoc(day2, part2)]
pub fn part2(input: &Vec<intcode::Value>) -> intcode::Value {
    for noun in 0..=99 {
        for verb in 0..=99 {
            let mut memory = input.clone();
            if intcode_process(&mut memory, noun, verb) == 19690720 {
                return 100 * noun + verb;
            }
        }
    }

    panic!("No matching input found");
}

#[cfg(test)]
mod tests {
    tests!(2, 5866663, 4259);
}
