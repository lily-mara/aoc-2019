use crate::util::DigitIter;
use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day4)]
pub fn gather_input(input: &str) -> (i32, i32) {
    let values: Vec<_> = input.split('-').map(|l| l.parse().unwrap()).collect();

    (values[0], values[1])
}

#[aoc(day4, part1)]
pub fn part1((low, high): &(i32, i32)) -> i32 {
    (*low..=*high)
        .map(|n| if is_valid(n) { 1 } else { 0 })
        .sum()
}

fn is_valid(num: i32) -> bool {
    let mut iter = DigitIter(num);

    let mut has_repeat_digits = false;
    let mut digit_count = 1;
    let mut last_digit = match iter.next() {
        Some(digit) => digit,
        None => return false,
    };

    for digit in iter {
        if digit == last_digit {
            has_repeat_digits = true;
        } else if last_digit < digit {
            return false;
        }

        last_digit = digit;
        digit_count += 1;
    }

    has_repeat_digits && digit_count == 6
}

fn is_valid2(num: i32) -> bool {
    let mut iter = DigitIter(num);

    let mut has_repeat_digits = false;
    let mut digit_count = 1;
    let mut last_digit = match iter.next() {
        Some(digit) => digit,
        None => return false,
    };
    let mut digit_repeat = 0;

    for digit in iter {
        if digit == last_digit {
            digit_repeat += 1;
        } else if last_digit < digit {
            return false;
        } else {
            if digit_repeat == 1 {
                has_repeat_digits = true;
            }
            digit_repeat = 0;
        }

        digit_count += 1;
        last_digit = digit;
    }

    if digit_repeat == 1 {
        has_repeat_digits = true;
    }

    has_repeat_digits && digit_count == 6
}

#[aoc(day4, part2)]
pub fn part2((low, high): &(i32, i32)) -> usize {
    (*low..=*high)
        .map(|n| if is_valid2(n) { 1 } else { 0 })
        .sum()
}

#[cfg(test)]
mod tests {
    tests!(4, 1729, 1172);
}
